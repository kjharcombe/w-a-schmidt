===========================================================
             "Adobe Minion" font pack for LaTeX
============================================================
                                                  2004-04-13
                                               WalterSchmidt
                                       w.a.schmidt(@)gmx.net
                                               

This bundle includes all files required to use the "Adobe
Minion" Type1 fonts with LaTeX on the Un*x or PC platform.
It does _not_ include the actual Type1 fonts, which are to
be purchased from Adobe. (packages # 143 and 144)

   Math typesetting is supported in two flavors:

   First, you may use the free Euler typefaces, wich are
   available with any decent TeX system.

|  Alternatively, use the commercial MathTime and MathTime
|  Plus fonts from Y&Y <http://www.yandy.com/mathtime.htm>.
|  Additional support files to mix Minion with MathTime are
|  provided in the present collection.

   After installing of the present collecion, more
   information can be found in the files
   texmf/do/fonts/adobe/pmn.txt and ./mathpmnt.pdf

The below installation instructions assume a TDS-compliant
TeX system, such as teTeX, MikTeX or VTeX/Free.  Yet they
may not exactly fit your particular TeX system; please,
consult its documentation, too!  The directory name "texmf"
refers to the root directory of a TDS directory tree.  In
case your TeX systems has more than one directory tree, its
documentation should tell you where to install new files.

|  On VTeX, you should not install both Linotype's and
|  Adobe's Minion fonts simultaneously.  Rationale:  Part of
|  the fonts in both collections are identical, and
|  installing distinct font files with the same internal
|  FontNames is likely to confuse the PostScript interpreter
|  (GeX)



What's new?
-----------
2004-04-13
  mathpmnt.sty, version 2.1: Math alphabet \mathbold removed;
  please, use \bm (from package bm.sty) instead.
  
2004-03-14
  Changed location of font map file pmn.ali in the
  distribution; it is now distributed both in dvips/config
  and in fonts/map/dvips, so as to comply with the future 
  teTeX.
  Package mathpmnt.sty, new version 2.0a; Bug fix regarding
  the definitions of the upright Greek characters.
  
2003-12-01
  Virtual fonts for italic smallcaps (font shape "scit")
  added; they may be useful in conjunction with H.Harders'
  macro package slantsc.sty.  
  Minor bug fix in the kerning table of the regular
  smallcaps.
 
2003-10-18
  Virtual mathpmnt fonts fixed:   With previous versions,
  the letters v and w in math mode were taken from Times
  rather than Minion.
  
2003-10-17
  The package mathpmnt.sty provides a bold-cursive math
  alphabet now.  The documentation (mathpmnt.pdf) has been
  adapted accordingly.  (The previous relase came with a
  wrong version of this document, anyway.)  The virtual 
  fonts are unchanged.
  
2003-08-19
  Fixed kerning of Lsmall-Ysmall in the smallcaps.

2003-07-22
  A full upright Greek alphabet is now available with 
  mathpmnt.sty and the related virtual math fonts.
  The text fonts provide (faked) Euro symbols.
  All VFs and metrics heve been remade using the latest 
  fontinst version 1.926.

2003-07-08
  Improved kerning data for german umlauts (once again ;-)
  All kerning pairs < 5 units removed.

2003-06-01
  Missing files added to ZIP archive.
  (sorry for the bug :-)  
   
2003-05-29
  Improved kerning (e-V, Y-g, Y-s)
  
2003-05-12 (bug fixing release)
  Missing tfm and vf files added.
  Improved kerning of the text fonts.
  Slightly improved spacing in math mode (mathpmnt.sty).
  
2003-05-06:  
  Math support added (mathpmnt.sty).  
  Slightly improved kerning data, particularly in the 
  smallcaps fonts.
  Changed location of the VTeX font mapping file pmn.ali.

2002-06-03:  
  Improved kerning data for the German language.  
  OT1 encoding is no longer supported.  
  ZIP archives reflects TDS structure.

2001-04-07: 
  Minion-Ormaments added



Removing absolete files
-----------------------
* In case you have installed the 2001-04-07 release of this
bundle, please delete the file

  texmf/doc/latex/adobe/minion.txt
  
and the directories

  texmf/tex/latex/adobe/minion
  texmf/fonts/tfm/adobe/minion
  texmf/fonts/vf/adobe/minion

before installing the present release.

* In case you have installed _any_ previous release and are
using VTeX/Free 8.x, please delete the file

  texmf/vtex/config/pmn.ali.  
  
now.



Installing the Type1 font files
-------------------------------
The Type1 fonts, as supplied by Adobe, are to be renamed for
use with TeX:

Adobe file name   TeX file name   PostScript FontName

mob_____.pfb      pmnb8a.pfb      Minion-Bold
mobi____.pfb      pmnbi8a.pfb     Minion-BoldItalic
mods____.pfb      pmnrd8a.pfb     Minion-DisplayRegular
modsi___.pfb      pmnrid8a.pfb    Minion-DisplayItalic
moi_____.pfb      pmnri8a.pfb     Minion-Italic
morg____.pfb      pmnr8a.pfb      Minion-Regular
mosb____.pfb      pmns8a.pfb      Minion-Semibold
mosbi___.pfb      pmnsi8a.pfb     Minion-SemiboldItalic
mjb_____.pfb      pmnb8x.pfb      MinionExp-Bold
mjbi____.pfb      pmnbi8x.pfb     MinionExp-BoldItalic
mjds____.pfb      pmnrd8x.pfb     MinionExp-DisplayRegular
mjdsi___.pfb      pmnrid8x.pfb    MinionExp-DisplayItalic
mji_____.pfb      pmnri8x.pfb     MinionExp-Italic
mjrg____.pfb      pmnr8x.pfb      MinionExp-Regular
mjsb____.pfb      pmns8x.pfb      MinionExp-Semibold
mjsbi___.pfb      pmnsi8x.pfb     MinionExp-SemiboldItalic
moor____.pfb      pmnrp.pfb       Minion-Ornaments

Copy them to the directory

  texmf/fonts/type1/adobe/minion

of your TeX system.  Rename the .afm files accordingly 
and copy them to:

  texmf/fonts/afm/adobe/minion

Most likely, you will have to create these directories
first.



Installing the TeX support files from the archive pmn.zip
---------------------------------------------------------
Unpack the ZIP archive pmn.zip in the directory "texmf" 
of your TeX system; thus, all files will be copied to the
appropriate directories.



Updating the filename database
------------------------------
Many TeX systems require manual updating of a "filename
database" after adding of new files.  Please, consult the
documentation of your TeX system!



Configuring your TeX system
---------------------------
|
| You need not repeat this step, when updating from a
| previous release of this collection!
|

The present distribution comprises several font map files
for the Adobe Minio fonts.  You need to configure
your TeX system so that these files are actually used.  The
required steps depend on the particular TeX system.
Particular sets of instructions are provided below for the
following systems:

  * teTeX      (v2.0 and above)
  * VTeX/Free  (v8.44 and above)

With other TeX systems such as MikTeX, consult the related
documentation how to install an additional font map file.
The name of the map file to be used for Minion is "pmn.map".
Two copies of this file reside in the directories
texmf/dvips/config and texmf/fonts/map/dvips.

Configuring teTeX
-----------------
Additional font map files (here pmn.map) are installed using
the shell script "updmap".  With teTeX-2.0 and later (or
teTeX-beta as of June 2002 and later) execute the following
commands:

  texhash
  updmap --enable Map pmn.map

With earlier versions of teTeX, consult its documentation
how to install an additional map file.

Configuring VTeX/Free
---------------------
Make VTeX read the additional font map ("aliasing") file
pmn.ali.  This is usually accomplished by putting an
appropriate record ito each of the configuration files

  texmf/vtex/config/pdf.fm
and
  texmf/vtex/config/ps.fm

The name "pmn.ali" is to be added to the TYPE1 section of
the above-mentioned files:

TYPE1 {
  ...
  pmn.ali
  }
  


Using the Adobe Minion fonts with LaTeX
---------------------------------------
See the files minion.txt and mathpmnt.pdf, which reside in
the directory texmf/doc/fonts/adobe.



Legal notice
------------
The bundle "Adobe Minion for LaTeX "is made up from
the files pmn.txt and pmn.zip.

  Copyright (c) 2001--2004 Walter Schmidt

It may be distributed and/or modified under the conditions
of the LaTeX Project Public License, either version 1.3 of
this license or (at your option) any later version.  
The latest version of this license is in
<http://www.latex-project.org/lppl.txt> and version 1.3 or
later is part of all distributions of LaTeX version
2003/12/01 or later.
This bundle has the LPPL maintenance status "maintained".

  
== finis
