============================================================
        Bitstream Letter Gothic font pack for LaTeX
============================================================
                                                  2004-07-22
                                              Walter Schmidt
                                      w.a.schmidt(at)gmx.net


This bundle includes all required files for using the
Bitstream "Letter Gothic 12 Pitch" Type1 fonts with LaTeX on
the Un*x or PC platform.  It does _not_ include the actual
Type1 fonts, which are to be purchased from Bitstream.

The below installation instructions assume a TDS-compliant
TeX system, such as teTeX or VTeX/Free.  Yet they may not
exactly fit your particular TeX system; please, consult its
documentation, too!  The directory name "texmf" refers to
the root directory of a TDS directory tree.  In case your
TeX systems has more than one directory tree, its
documentation should tell you where to install new files.



What's new?
-----------
2004-07-22
  changed location of font mapping files wrt/ TDS 1 specs

2003-03-02
  fixed the asterisk characters, so that they are vertically
  centered now; changed directory layout



Removing obsolete files
-----------------------
In case you install this collection over any previous 
release, delete the file texmf/vtex/config/blg.ali now.



Installing the Type1 font files
-------------------------------
The Type1 font files are to be renamed for use with TeX.
The names of the original files may vary, depending on 
whether you have got the fonts from Bitsream or Corel:

Bitstream:     Corel:            TeX:
0414a___.pfb   letgot12.pfb  ->  blgr8a.pfb
0532a___.pfb   legot12i.pfb  ->  blgri8a.pfb
0533a___.pfb   legot12b.pfb  ->  blgb8a.pfb
0543a___.pfb   lego12bi.pfb  ->  blgbi8a.pfb
          
The .pfb files are to be copied to the directory

  texmf/fonts/type1/bitstrea/lettrgth

of your TeX system.  The related .afm files should also be
renamed accordingly and installed into the directory

  texmf/fonts/afm/bitstrea/lettrgth

You may need to create these directories first.  



Installing the TeX support files from the archive blg.zip
---------------------------------------------------------
Unpack the ZIP archive blg.zip in the directory "texmf" of
your TeX system; thus, all files will be copied to the
appropriate directories.



Configuring your TeX system
---------------------------
|
| You need not repeat this step, when updating from a
| previous release of this collection!
|

The present distribution comprises several font map files
for the Bitstream LetterGothic fonts.  You need to configure
your TeX system so that these files are actually used.  
The required steps depend on the particular TeX system.
Particular sets of instructions are provided below for the
following systems:

  * teTeX
  * VTeX/Free

With other TeX systems such as MikTeX, consult the related
documentation how to install an additional font map file.
The name of the map file to be used for LetterGothic is
"blg.map".  Two copies of this file reside in the
directories texmf/dvips/config and texmf/fonts/map/dvips.

Configuring teTeX
-----------------
Additional font map files (here blg.map) are installed using
the shell script "updmap".  With teTeX-2.0 and later (or
teTeX-beta as of June 2002 and later) execute the following
commands:

  texhash
  updmap --enable Map blg.map

With earlier versions of teTeX, consult its documentation
about how to add new map files to the system.

Configuring VTeX/Free
---------------------
Make VTeX read the additional font map ("aliasing") file
blg.ali.  This is usually accomplished by putting an
appropriate record ito each of the configuration files

  texmf/vtex/config/pdf.fm
and
  texmf/vtex/config/ps.fm

The name "blg.ali" is to be added to the TYPE1 section of
the above-mentioned files:

TYPE1 {
  ...
  blg.ali
  }
  
In case you are still using VTeX/Free 7.x (as opposed to 
v8.x), copy the .ali file from texmf/fonts/map/vtex to the
directory texmf/vtex/config.



Removing obsolete files
-----------------------
In case you install this package over the version from
2001-02-02, the directory

  texmf/tex/latex/bitstrea/lettrgth/

and the file

  texmf/doc/latex/bitstrea/lettrgth.pdf .

are still to be deleted manually.  Rationale:  The macro
files reside in the directory texmf/tex/latex/blg now, 
and the documentation has been moved to the directory
texmf/doc/fonts/bitstrea.  Thus, unpacking of the .zip
archiv alone would not overwrite the obsolete files.



Updating the filename database
------------------------------
Certain TeX systems require manually updating of a "filename
database" after adding of new files.  Please, consult the
documentation of your TeX system!



Using the Letter Gothic fonts with LaTeX
-----------------------------------------
See the file lettrgth.txt, which should reside in the
directory texmf/doc/fonts/bitstrea.


Legal notice
------------
The Letter Gothic font pack is made up from the files
blg.txt and blg.zip.

  Copyright (c) 2002--2004 Walter Schmidt

It may be distributed and/or modified under the conditions
of the LaTeX Project Public License, either version 1.3 of
this license or (at your option) any later version.  
The latest version of this license is in
<http://www.latex-project.org/lppl.txt> and version 1.3 or
later is part of all distributions of LaTeX version
2003/12/01 or later.
This bundle has the LPPL maintenance status "maintained".

== finis
