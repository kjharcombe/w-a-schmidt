===========================================================
             "Adobe Garamond" font pack for LaTeX
============================================================
                                                  2004-08-08
                                               WalterSchmidt
                                      w.a.schmidt(at)gmx.net
                                               

This bundle includes all files required to use the "Adobe
Garamond" Type1 fonts with LaTeX on the Un*x or PC platform.
It does _not_ include the actual Type1 fonts, which are to
be purchased from Adobe (packages # 143 and 144) or Linotype.
Note that you need a recent version of the fonts, which 
includes the Euro symbol!  In particular, the following fonts
are supported for use with TeX:

Adobe Garamond Bold Italic Expert
Adobe Garamond Bold Expert
Adobe Garamond Italic Expert
Adobe Garamond Regular Expert
Adobe Garamond-Semibold  
Adobe Garamond Semibold Italic Expert
Adobe Garamond Semibold Expert
Adobe Garamond Regular
Adobe Garamond Italic
Adobe Garamond Semibold Italic
Adobe Garamond Bold
Adobe Garamond Bold Italic

The below installation instructions assume a TDS-compliant
TeX system, such as teTeX, MikTeX or VTeX/Free.  Yet they
may not exactly fit your particular TeX system; please,
consult its documentation, too!  The directory name "texmf"
refers to the root directory of a TDS directory tree.  In
case your TeX systems has more than one directory tree, its
documentation should tell you where to install new files.



What's new?
-----------

2004-08-08
   Fixed a potential bug in the macro package

2003-06-01
  Works with both Adobe's and Linotype's fonts
  
2003-05-29
  Kerning data thoroughly revised.
  Corrected excessive height of certain accented letters.
  Changed location of the VTeX font mapping file pad.ali.
  
2002-11-30
  The Euro symbol is supported. 
  Improved kerning.

2002-09-01
  Improved kernkng.
  Obsolete OT1 encoding is no longer supported.
  Changed directory layout

2001-05-21
  New package xagaramon.sty repleces agaramox.sty and
  agaramoj.sty.



Installing the Type1 font files from Adobe
------------------------------------------
Skip this section, if you have got the font files from
Linotype!

The Type1 fonts, as supplied by Adobe, are to be renamed for
use with TeX:

FontName:                   Adobe file name:  TeX file name:
AGaramond-Bold              gdb_____.pfb      padb8a.pfb
AGaramond-BoldItalic        gdbi____.pfb      padbi8a.pfb
AGaramond-Italic            gdi_____.pfb      padri8a.pfb
AGaramond-Regular           gdrg____.pfb      padr8a.pfb
AGaramond-Semibold          gdsb____.pfb      pads8a.pfb
AGaramond-SemiboldItalic    gdsbi___.pfb      padsi8a.pfb
AGaramondExp-Bold           geb_____.pfb      padb8x.pfb
AGaramondExp-BoldItalic     gebi____.pfb      padbi8x.pfb
AGaramondExp-Italic         gei_____.pfb      padri8x.pfb
AGaramondExp-Regular        gerg____.pfb      padr8x.pfb
AGaramondExp-Semibold       gesb____.pfb      pads8x.pfb
AGaramondExp-SemiboldItalic gesbi___.pfb      padsi8x.pfb

Copy them to the directory

  texmf/fonts/type1/adobe/agaramon

of your TeX system.  Rename the .afm files accordingly and
copy them to:

  texmf/fonts/afm/adobe/agaramon

Most likely, you will have to create these directories
first.


Installing the Type1 font files from Linotype
---------------------------------------------
Skip this section, if you have got the font files from
Adobe!

The Type1 fonts, as supplied by Linotype, are to be renamed
for use with TeX:

FontName:             Linotype file name:     TeX file name:
AGaramondLT-Bold            lte50570.pfb      ladb8a.pfb     
AGaramondLT-BoldItalic      lte50571.pfb      ladbi8a.pfb    
AGaramondLT-Italic          lte50567.pfb      ladri8a.pfb    
AGaramondLT-Regular         lte50566.pfb      ladr8a.pfb     
AGaramondLT-Semibold        lte50568.pfb      lads8a.pfb     
AGaramondLT-SemiboldItalic  lte50569.pfb      ladsi8a.pfb    
AGaramondExp-Bold           geb_____.pfb      ladb8x.pfb     
AGaramondExp-BoldItalic     gebi____.pfb      ladbi8x.pfb    
AGaramondExp-Italic         gei_____.pfb      ladri8x.pfb    
AGaramondExp-Regular        gerg____.pfb      ladr8x.pfb     
AGaramondExp-Semibold       gesb____.pfb      lads8x.pfb     
AGaramondExp-SemiboldItalic gesbi___.pfb      ladsi8x.pfb    

Copy them to the directory

  texmf/fonts/type1/linotype/agaramon

of your TeX system.  Rename the .afm files accordingly and
copy them to:

  texmf/fonts/afm/linotype/agaramon

Most likely, you will have to create these directories
first.



Installing the TeX support files from the archive pad.zip
---------------------------------------------------------
Unpack the ZIP archive pad.zip in the directory "texmf" of
your TeX system; thus, all files will be copied to the
appropriate directories.



Updating the filename database
------------------------------
Many TeX systems require manual updating of a "filename
database" after adding of new files.  Please, consult the
documentation of your TeX system!



Configuring your TeX system
---------------------------
The following steps vary, according to your TeX system.
There are different sets of instructions provided for

  * teTeX (2.0 and later)
  * VTeX/Free (8.44 and later)

With other TeX systems such as MikTeX, consult the related
documentation how to install an additional font map file.
The name of the map file to be used for AGaramond is
"pad.map" (if you have got the fonts from Adobe) or
"pad-LT.map", you you have installed the Linotype fonts.
Two copies of each file reside in the directories
texmf/dvips/config and texmf/fonts/map /dvips.

Configuring teTeX
-----------------
Additional font map files (here:  pad-map or pad-LT.map) are
installed using the shell script "updmap".  With teTeX-2.0
and later (or teTeX-beta as of June 2002 and later) execute
the following commands:

  texhash
  updmap --enable Map pad.map
  
if you have got the fonts from Adobe, or

  texhash
  updmap --enable Map pad-LT.map

for the fonts from Linotype.

With earlier versions of teTeX, consult its documentation
how to use the "updmap" script.  

Configuring VTeX/Free
---------------------
Make VTeX read the additional font map ("aliasing") file
pad.ali (for the Adobe fonts) or pad-LT.ali (Linotype).
This is usually accomplished by putting a record for the
particular .ali file into each of the configuration files

  texmf/vtex/config/pdf.fm
and
  texmf/vtex/config/ps.fm

The name "pad.ali" or "pad-LT.ali" is to be added to the
TYPE1 section of the above-mentioned files, e.g.:

TYPE1 {
  ...
  pad.ali
  }
  



Using the Adobe Garamond fonts with LaTeX
-----------------------------------------
See the file agaramon.txt, which resides in the directory
texmf/doc/fonts/adobe.



Legal notice
------------
The bundle "Adobe Garamond for LaTeX "is made up from
the files pad.txt and pad.zip.

  Copyright (c) 2001--2003 Walter Schmidt

It may be distributed and/or modified under the conditions
of the LaTeX Project Public License, either version 1.3 of
this license or (at your option) any later version.
The latest version of this license is in
<http://www.latex-project.org/lppl.txt> and version 1.3 or
later is part of all distributions of LaTeX version
2003/12/01 or later.
This bundle has the LPPL maintenance status "maintained".
  

== finis
