===========================================================
      Linotype ITC Officina Sans font pack for LaTeX
============================================================
                                                  2003-05-31
                                               WalterSchmidt
                                      w.a.schmidt(at)gmx.net
                                               

This bundle includes all files required to use Linotype's
font family "ITC Officina Sans by ITC" in Type1 (PostScript)
format with LaTeX on the Un*x or PC platform.  It does _not_
include the actual font files, which are to be purchased
from Linotype.

Notice that Linotype offers two different digitizations of
the Officina Sans typeface family:  "ITC Officina Sans" and
"ITC Officina Sans by ITC".  The present collection supports
only the _latter_ package, which can be regarded as the
"original", and which comprises more fonts, including also
smallcaps.  

The TeX support files cover only the weights "Book" and
"Bold", including the related smallcaps.  They refer to the
version of the fonts that was released with version 1.6 of
the Linotype Library and on the Gold Edition CD-ROM 1.6.
They cannot be used with earlier versions.

The below installation instructions assume a TDS-compliant
TeX system, such as teTeX, MikTeX or VTeX/Free.  Yet they
may not exactly fit your particular TeX system; please,
consult its documentation, too!  The directory name "texmf"
refers to the root directory of a TDS directory tree.  In
case your TeX systems has more than one directory tree, its
documentation should tell you where to install new files.



Installing the Type1 font files
-------------------------------
The Type1 fonts, as supplied by Linotype, are to be renamed
for use with TeX:

FontName:                 Linotype:         TeX:          
OfficinaSanITC-Bold       lt_70808.pfb      lo9b8a.pfb
OfficinaSanITC-BoldItal   lt_70811.pfb      lo9bi8a.pfb
OfficinaSanITC-Book       lt_70813.pfb      lo9r8a.pfb
OfficinaSanITC-BookItal   lt_70816.pfb      lo9ri8a.pfb
OfficinaSanITC-BoldSC     ofsnbsc_.pfb      lo9bc8a.pfb
OfficinaSanITC-BookSC     ofsnwsc_.pfb      lo9rc8a.pfb

Copy them to the directory

  texmf/fonts/type1/linotype/officina

of your TeX system.  Rename the .afm files accordingly and
copy them to:

  texmf/fonts/afm/linotype/officina

Most likely, you will have to create these directories
first.



Installing the TeX support files from the archive lo9.zip
---------------------------------------------------------
Unpack the ZIP archive lo9.zip in the directory "texmf" 
of your TeX system; thus, all files will be copied to the
appropriate directories.



Updating the filename database
------------------------------
Many TeX systems require manual updating of a "filename
database" after adding of new files.  Please, consult the
documentation of your TeX system!



Configuring your TeX system
---------------------------
The present distribution comprises several font map files
for the Linotype ITC-Officina fonts.  You need to configure
your TeX system so that these files are actually used.  
The required steps depend on the particular TeX system.
Particular sets of instructions are provided below for the
following systems:

  * teTeX
  * VTeX/Free

With other TeX systems such as MikTeX, consult the related
documentation how to install an additional font map file.
The name of the map file to be used for ITC Officina is
"lo9.map".  Two copies of this file reside in the
directories texmf/dvips/config and texmf/fonts/map/dvips.

Configuring teTeX
-----------------
Additional font map files (here lo9.map) are installed using
the shell script "updmap".  With teTeX-2.0 and later (or
teTeX-beta as of June 2002 and later) execute the following
commands:

  texhash
  updmap --enable Map lo9.map

With earlier versions of teTeX, consult its documentation
about how to add new map files to the system.

Configuring VTeX/Free
---------------------
Make VTeX read the additional font map ("aliasing") file
lo9.ali.  This is usually accomplished by putting an
appropriate record ito each of the configuration files

  texmf/vtex/config/pdf.fm
and
  texmf/vtex/config/ps.fm

The name "<nfssfam>.ali" is to be added to the TYPE1 section of
the above-mentioned files:

TYPE1 {
  ...
  lo9.ali
  }
  
In case you are still using VTeX/Free 7.x (as opposed to 
v8.x), copy the .ali file from texmf/fonts/map/vtex to the 
directory texmf/vtex/config.



Using the Linotype Syntax fonts with LaTeX
----------------------------------------
See the file officina.txt, which resides in the directory
texmf/doc/fonts/linotype.



Legal notice
------------
The bundle "Linotype ITC Officina for LaTeX" is made up from
the files lo9.txt and lo9.zip.

  Copyright (c) 2003 Walter Schmidt

It may be distributed and/or modified under the conditions
of the LaTeX Project Public License, either version 1.3 of
this license or (at your option) any later version.  
The latest version of this license is in
<http://www.latex-project.org/lppl.txt> and version 1.3 or
later is part of all distributions of LaTeX version
2003/12/01 or later.
This bundle has the LPPL maintenance status "maintained".


== finis
