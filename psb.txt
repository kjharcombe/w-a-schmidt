============================================================
          "Adobe Sabon" font pack as of 2003-10-22
============================================================
                                              Walter Schmidt
                                       w.a.schmidt(@)gmx.net


This bundle includes all required files for using Adobe's
Sabon Type1 fonts with LaTeX on the Un*x or PC platform.  It
does _not_ include the actual Type1 fonts, which are to be
purchased.  Note that you need a recent version of the
fonts, which includes the Euro symbol!  See below for the
list of particular font files needed.

The below installation instructions assume a TDS-compliant
TeX system, such as teTeX, MikTeX or VTeX/Free.  Yet they
may not exactly fit your particular TeX system; please,
consult its documentation, too!  The directory name "texmf"
refers to the root directory of a TDS directory tree.  In
case your TeX systems has more than one directory tree, its
documentation should tell you where to install new files.



News
----
2003-10-22
  All virtual fonts re-made using latest fontinst,
  thus fixing various deficiencies;
  slightly improved kerning tables.



Bugs and problems
-----------------
The original font metrics as supplied by Adobe are lacking
proper kerning data for accented letters, umlauts and
typical character pairs that occur in other languages than
English.  The kerning tables in the TeX metric files have
been somewhat corrected for the German language, but no
further languages are particularly covered so far.



Removing obsolete files
-----------------------
In case you have installed a previous release of this
collection, please remove manually the file 
texmf/vtex/config/psb.ali before installing the present 
release.



Installing the Type1 font files
-------------------------------
The Type1 font files are to be renamed for use with TeX.
The names of the original files may vary, so the following
table tells the related PostScript font names, too:

FontName:            Adobe:          TeX:          
Sabon-Roman          sar_____.pfb    psbr8a.pfb    
Sabon-Italic         sai_____.pfb    psbri8a.pfb   
Sabon-Bold           sab_____.pfb    psbb8a.pfb    
Sabon-BoldItalic     sabi____.pfb    psbbi8a.pfb   
Sabon-RomanSC        sarsc___.pfb    psbrc8a.pfb   
Sabon-ItalicOsF      saiof___.pfb    psbrij8a.pfb  
Sabon-BoldOsF        sabof___.pfb    psbbj8a.pfb   
Sabon-BoldItalicOsF  sabio___.pfb    psbbij8a.pfb  
          
The .pfb files are to be copied to the directory

  texmf/fonts/type1/adobe/sabon

of your TeX system.  The related .afm files should also be
renamed accordingly and installed into the directory

  texmf/fonts/afm/adobe/sabon

Most likely, you need to create these directories first.  



Installing the TeX support files from the archive psb.zip
---------------------------------------------------------
Unpack the ZIP archive psb.zip in the directory "texmf" of
your TeX system; thus, all files will be copied to the
appropriate directories.



Updating the filename database
------------------------------
Certain TeX systems require manually updating of a "filename
database" after adding of new files.  Please, consult the
documentation of your TeX system!



Configuring your TeX system
---------------------------
|
| You need not repeat this step, when updating from a
| previous release of this collection!
|

The present distribution comprises several font map files
for the Adobe Sabon fonts.  You need to configure
your TeX system so that these files are actually used.  
The required steps depend on the particular TeX system.
Particular sets of instructions are provided below for the
following systems:

  * teTeX
  * VTeX/Free

With other TeX systems such as MikTeX, consult the related
documentation how to install an additional font map file.
The name of the map file to be used for Sabon is
"psb.map".  Two copies of this file reside in the
directories texmf/dvips/config and texmf/fonts/map/dvips.

Configuring teTeX
-----------------
Additional font map files (here psb.map) are installed using
the shell script "updmap".  With teTeX-2.0 and later (or
teTeX-beta as of June 2002 and later) execute the following
commands:

  texhash
  updmap --enable Map psb.map

With earlier versions of teTeX, consult its documentation
how to install an additional map file.

Configuring VTeX/Free
---------------------
Make VTeX read the additional font map ("aliasing") file
psb.ali.  This is usually accomplished by putting an
appropriate record ito each of the configuration files

  texmf/vtex/config/pdf.fm
and
  texmf/vtex/config/ps.fm

The name "psb.ali" is to be added to the TYPE1 section of
the above-mentioned files:

TYPE1 {
  ...
  psb.ali
  }
  
In case you are still using VTeX/Free 7.x (as opposed to 
v8.x), copy the file psb.ali from texmf/fonts/map/vtex to 
the directory texmf/vtex/config.



Using the Sabon fonts with LaTeX
--------------------------------
See the file sabon.txt, which resides in the directory
texmf/doc/fonts/adobe.



Legal notice
------------
The bundle "Adobe Sabon for LaTeX "is made up from the files
psb.txt and psb.zip.

  Copyright (c) 2003 Walter Schmidt

It may be distributed and/or modified under the conditions
of the LaTeX Project Public License, either version 1.3 of
this license or (at your option) any later version.  
The latest version of this license is in
<http://www.latex-project.org/lppl.txt> and version 1.3 or
later is part of all distributions of LaTeX version
2003/12/01 or later.  
This bundle has the LPPL maintenance status "maintained".

== finis
