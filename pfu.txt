===========================================================
             "Adobe Futura" font pack for LaTeX
============================================================
                                                  2004-09-28
                                               WalterSchmidt
                                      w.a.schmidt(at)gmx.net
                                               

This bundle includes all files required to use the "Adobe
Futura" Type1 fonts with LaTeX on the Un*x or PC platform.
It does _not_ include the actual Type1 fonts, which are to
be purchased from either Adobe or Linotype.

The following fonts of the Futura family are supported:

 Futura Light
 Futura Light Oblique
 Futura Book
 Futura Book Oblique
 Futura Bold
 Futura Bold Oblique
 Futura Condensed Bold
 Futura Condensed Bold Oblique

The below installation instructions assume a TDS-compliant
TeX system, such as teTeX, MikTeX or VTeX/Free.  Yet they
may not exactly fit your particular TeX system; please,
consult its documentation, too!  The directory name "texmf"
refers to the root directory of a TDS directory tree.  In
case your TeX systems has more than one directory tree, its
documentation should tell you where to install new files.



What's new?
-----------
2004-09-28
  completely new virtual fonts, made using fontinst 1.926;
  bold-condensed fonts are supported;
  macro package to support optional scaling;
  obsolete OT1 encoding is no longer supported;
  Euro symbols



Removing obsolete files
-----------------------
If you have installed a pre-2004 release of this collection,
remove it entirely before installing the present one.  
In particular, delete the directories

  texmf/tex/latex/pfu
  texmf/fonts/tfm/adobe/futura
  texmf/fonts/vf/adobe/futura

and the files

  texmf/doc/fonts/adobe/futura.txt
  texmf/dvips/config/pfu.map
  texmf/vtex/config/pfu.map
  
and remove the font mapping information for the Futura fonts
from the configuration of the TeX applications.



Installing the Type1 font files from Adobe
------------------------------------------
Skip this section, if you have got the font files from
Linotype!

The Type1 fonts, as supplied by Adobe, are to be renamed for
use with TeX:

FontName:                 Adobe file name:  TeX file name:
Futura-Bold                  fub_____.pfb   pfub8a.pfb   
Futura-BoldOblique           fubo____.pfb   pfubo8a.pfb  
Futura-Light                 ful_____.pfb   pful8a.pfb   
Futura-LightOblique          fulo____.pfb   pfulo8a.pfb  
Futura-Book                  fuw_____.pfb   pfuk8a.pfb   
Futura-BookOblique           fuwo____.pfb   pfuko8a.pfb  
Futura-CondensedBold         fucb____.pfb   pfub8ac.pfb  
Futura-CondensedBoldOblique  fucbo___.pfb   pfubo8ac.pfb 
Copy them to the directory

  texmf/fonts/type1/adobe/futura

of your TeX system.  Rename the .afm files accordingly and
copy them to:

  texmf/fonts/afm/adobe/futura

Most likely, you will have to create these directories
first.


Installing the Type1 font files from Linotype
---------------------------------------------
Skip this section, if you have got the font files from
Adobe!

The Type1 fonts, as supplied by Linotype, are to be renamed
for use with TeX:

FontName:              Linotype file name:   TeX file name:
FuturaLT-Light                lte50150.pfb   lful8a.pfb    
FuturaLT-LightOblique         lte50151.pfb   lfulo8a.pfb   
FuturaLT-Book                 lte50152.pfb   lfuk8a.pfb    
FuturaLT-BookOblique          lte50153.pfb   lfuko8a.pfb   
FuturaLT-Bold                 lte50154.pfb   lfub8a.pfb    
FuturaLT-BoldOblique          lte50155.pfb   lfubo8a.pfb   
FuturaLT-CondensedBold        lte50196.pfb   lfub8ac.pfb   
FuturaLT-CondensedBoldOblique lte50197.pfb   lfubo8ac.pfb  

Copy them to the directory

  texmf/fonts/type1/linotype/futura

of your TeX system.  Rename the .afm files accordingly and
copy them to:

  texmf/fonts/afm/linotype/futura

Most likely, you will have to create these directories
first.



Installing the TeX support files from the archive pfu.zip
---------------------------------------------------------
Unpack the ZIP archive pfu.zip in the directory "texmf" of
your TeX system; thus, all files will be copied to the
appropriate directories.



Updating the filename database
------------------------------
Many TeX systems require manual updating of a "filename
database" after adding of new files.  Please, consult the
documentation of your TeX system!



Configuring your TeX system
---------------------------
The following steps vary, according to your TeX system.
There are different sets of instructions provided for

  * teTeX (2.0 and later)
  * VTeX/Free (8.44 and later)

With other TeX systems such as MikTeX, consult the related
documentation how to install an additional font map file.
The name of the map file to be used for Futura is
"pfu.map" (if you have got the fonts from Adobe) or
"pfu-LT.map", you you have installed the Linotype fonts.
Two copies of each file reside in the directories
texmf/dvips/config and texmf/fonts/map /dvips.

Configuring teTeX
-----------------
Additional font map files (here:  pfu-map or pfu-LT.map) are
installed using the shell script "updmap".  With teTeX-2.0
and later (or teTeX-beta as of June 2002 and later) execute
the following commands:

  texhash
  updmap --enable Map pfu.map
  
if you have got the fonts from Adobe, or

  texhash
  updmap --enable Map pfu-LT.map

for the fonts from Linotype.

With earlier versions of teTeX, consult its documentation
how to use the "updmap" script.  

Configuring VTeX/Free
---------------------
Make VTeX read the additional font map ("aliasing") file
pfu.ali (for the Adobe fonts) or pfu-LT.ali (Linotype).
This is usually accomplished by putting a record for the
particular .ali file into each of the configuration files

  texmf/vtex/config/pdf.fm
and
  texmf/vtex/config/ps.fm

The name "pfu.ali" or "pfu-LT.ali" is to be added to the
TYPE1 section of the above-mentioned files, e.g.:

TYPE1 {
  ...
  pfu.ali
  }
  



Using the Adobe Futura fonts with LaTeX
-----------------------------------------
See the file futura.txt, which resides in the directory
texmf/doc/fonts/adobe.



Legal notice
------------
The bundle "Adobe Futura for LaTeX "is made up from the
files pfu.txt and pfu.zip.

  Copyright (c) 2001--2004 Walter Schmidt

It may be distributed and/or modified under the conditions
of the LaTeX Project Public License, either version 1.3 of
this license or (at your option) any later version.
The latest version of this license is in
<http://www.latex-project.org/lppl.txt> and version 1.3 or
later is part of all distributions of LaTeX version
2003/12/01 or later.
This bundle has the LPPL maintenance status "maintained".
  

== finis
