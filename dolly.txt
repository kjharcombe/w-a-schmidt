===========================================================
                "Dolly" font pack for LaTeX
============================================================
                                                  2004-07-22
                                               WalterSchmidt
                                      w.a.schmidt(at)gmx.net
                                               

This bundle includes all required files to use the "Dolly"
Type1 fonts with LaTeX on Un*x or PC platform.  It does
_not_ include the actual Type1 fonts, which are distributed
by Underware <http://www.underware.nl>.

The below installation instructions assume a TDS-compliant
TeX system, such as teTeX, MikTeX or VTeX/Free.  The
directory name "texmf" refers to the root directory of the
TDS directory tree.  In case your TeX systems has more than
one such directory tree, the documentation should tell you
where to install new files.



Change log
-----------
2004-07-22
  Changed location of font map file for dvips wrt/ TDS 1.0.
  
2003-05-29
  Virtual fonts re-made with newer version of fontinst,
  thus avoiding many small deficiencies.  
  Changed location of font mapping file for VTeX, wrt/ TDS 
  1.0 and VTeX/Free 8.x.
  The font map files are unchanged.



Installing the Type1 font files
-------------------------------
The Type1 font files 

  dollb___.pfb
  dolli___.pfb
  dollr___.pfb
  dollsc__.pfb

are to be copied to the directory

  texmf/fonts/type1/undrware/dolly

of your TeX system, and the related .afm files should go
into

  texmf/fonts/afm/undrware/dolly .

You may need to create these directories first.  (Note that
the directory name is "undrware", not "underware".)  
On Unix, make sure that the file names are all lower-case!



Installing the TeX support files from the archive dolly.zip
-----------------------------------------------------------
Unpack the ZIP archive dolly.zip in the directory "texmf" 
of your TeX system; thus, all files will be copied to the
appropriate directories.



Configuring your TeX system
---------------------------
|
| You need not repeat this step, when updating from a
| previous release of this collection!
|

The present distribution comprises several font map files
for the Dolly fonts.  You need to configure your TeX system
so that these files are actually used.  The required steps
depend on the particular TeX system.  Particular sets of
instructions are provided below for the following systems:

  * teTeX
  * VTeX/Free

With other TeX systems such as MikTeX, consult the related
documentation how to install an additional font map file.
The name of the map file to be used for Dolly is
"dolly.map".  Two copies of this file reside in the
directories texmf/dvips/config and texmf/fonts/map/dvips.

Configuring teTeX
-----------------
Additional font map files (here dolly.map) are installed using
the shell script "updmap".  With teTeX-2.0 and later (or
teTeX-beta as of June 2002 and later) execute the following
commands:

  texhash
  updmap --enable Map dolly.map

With earlier versions of teTeX, consult its documentation
about how to add new map files to the system.

Configuring VTeX/Free
---------------------
Make VTeX read the additional font map ("aliasing") file
dolly.ali.  This is usually accomplished by putting an
appropriate record ito each of the configuration files

  texmf/vtex/config/pdf.fm
and
  texmf/vtex/config/ps.fm

The name "dolly.ali" is to be added to the TYPE1 section of
the above-mentioned files:

TYPE1 {
  ...
  dolly.ali
  }
  
In case you are still using VTeX/Free 7.x (as opposed to 
v8.x), copy the .ali file from texmf/fonts/map/vtex to the
directory texmf/vtex/config.



Updating the filename database
------------------------------
Many TeX systems require manual updating of a "filename
database" after adding of new files.  Please, consult the
documentation of your TeX system!



Using the Dolly fonts with LaTeX
---------------------------------
See the file dolly.txt, which should reside in the
directory texmf/doc/fonts/undrware.



Legal notice
------------
The Dolly font pack is made up from the files dolly.txt and
dolly.zip.

  Copyright (c) 2002, 2003 Walter Schmidt

It may be distributed and/or modified under the conditions
of the LaTeX Project Public License, either version 1.3 of
this license or (at your option) any later version.  
The latest version of this license is in
<http://www.latex-project.org/lppl.txt> and version 1.3 or
later is part of all distributions of LaTeX version
2003/12/01 or later.
This bundle has the LPPL maintenance status "maintained".


== finis
